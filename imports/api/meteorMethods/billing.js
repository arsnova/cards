import {Meteor} from "meteor/meteor";
import {Cardsets} from "../subscriptions/cardsets.js";

// Define gateway variable
var gateway;

Meteor.startup(function () {
	var env;
	// Pick Braintree environment based on environment defined in Meteor settings.
	if (Meteor.settings.public.env === 'Production') {
		env = Braintree.Environment.Production;
	} else {
		env = Braintree.Environment.Sandbox;
	}
	// Initialize Braintree connection:
	gateway = BrainTreeConnect({
		environment: env,
		publicKey: Meteor.settings.public.BT_PUBLIC_KEY,
		privateKey: Meteor.settings.private.BT_PRIVATE_KEY,
		merchantId: Meteor.settings.public.BT_MERCHANT_ID
	});
});

Meteor.methods({
	getClientToken: function (customerId) {
		return new Promise((resolve, reject) => {
			gateway.clientToken.generate({customerId: customerId}, function (error, response) {
				if (error) {
					reject(error);
				} else {
					resolve(response.clientToken);
				}
			});
		});
	},

	btGetPaymentMethod: function () {
		return new Promise((resolve, reject) => {
			var user = Meteor.users.findOne(this.userId);
			Meteor.call('btFindCustomer', user.customerId, function (error, customer) {
				if (error) {
					reject(error);
				} else {
					resolve(customer.paymentMethods);
				}
			});
		});
	},

	btUpdatePaymentMethod: function (nonceFromTheClient) {
		return new Promise((resolve, reject) => {
			Meteor.call('btCreateCustomer', nonceFromTheClient, function (error, btCustomer) {
				if (error) {
					reject(error);
				} else {
					var customerId;
					if (btCustomer === undefined) {
						customerId = Meteor.user().customerId;
					} else {
						customerId = btCustomer.customer.id;
					}

					gateway.paymentMethod.create({
						customerId: customerId,
						paymentMethodNonce: nonceFromTheClient,
						options: {
							makeDefault: true
						}
					}, function (createError, createResult) {
						if (createError) {
							reject(createError);
						} else {
							resolve(createResult);
						}
					});
				}
			});
		});
	},

	btCreateTransaction: function (nonceFromTheClient, cardset_id) {
		var cardset = Cardsets.findOne(cardset_id);
		return new Promise((reject, resolve) => {
			// Create our customer.
			Meteor.call('btCreateCustomer', nonceFromTheClient, function (error, btCustomer) {
				if (error) {
					reject(error);
				} else {
					var customerId;
					if (btCustomer === undefined) {
						customerId = Meteor.user().customerId;
					} else {
						customerId = btCustomer.customer.id;
					}
					gateway.transaction.sale({
						amount: cardset.price,
						paymentMethodNonce: nonceFromTheClient, // Generated nonce passed from client
						customerId: customerId,
						options: {
							submitForSettlement: true, // Payment is submitted for settlement immediatelly
							storeInVaultOnSuccess: true // Store customer in Braintree's Vault
						}
					}, function (saleError, saleResponse) {
						if (saleError) {
							reject(saleError);
						} else {
							Meteor.call('addPaid', cardset_id, cardset.price);
							Meteor.call('increaseUsersBalance', cardset.owner, cardset.reviewer, cardset.price);
							resolve(saleResponse.clientToken);
						}
					});
				}
			});
		});
	},

	btCreateCredit: function (nonceFromTheClient) {
		var user = Meteor.users.findOne(this.userId);
		return new Promise((resolve, reject) => {
			// Create our customer.
			Meteor.call('btCreateCustomer', nonceFromTheClient, function (error, btCustomer) {
				if (error) {
					reject(error);
				} else {
					var customerId;
					if (btCustomer === undefined) {
						customerId = Meteor.user().customerId;
					} else {
						customerId = btCustomer.customer.id;
					}

					// Make new credit
					gateway.transaction.credit({
						amount: user.balance,
						paymentMethodNonce: nonceFromTheClient, // Generated nonce passed from client
						customerId: customerId,
						options: {
							submitForSettlement: true, // Payment is submitted for settlement immediatelly
							storeInVaultOnSuccess: true // Store customer in Braintree's Vault
						}
					}, function (creditError, creditResponse) {
						if (creditError) {
							reject(creditError);
						} else {
							resolve(creditResponse.clientToken);
						}
					});
				}
			});
		});
	},

	btSubscribe: function (nonce, plan) {
		return new Promise((resolve, reject) => {
			// Create our customer.
			Meteor.call('btCreateCustomer', nonce, function (error, btCustomer) {
				if (error) {
					reject(error);
				} else {
					var customerId;
					if (btCustomer === undefined) {
						customerId = Meteor.user().customerId;
					} else {
						customerId = btCustomer.customer.id;
					}
					// Setup a subscription for our customer.
					Meteor.call('btCreateSubscription', customerId, plan, function (subscriptionError, subscriptionResponse) {
						if (subscriptionError) {
							reject(subscriptionError);
						} else {
							try {
								var customerSubscription = {
									customerId: customerId,
									subscription: {
										plan: plan,
										status: subscriptionResponse.subscription.status,
										billingDate: subscriptionResponse.subscription.nextBillingDate
									},
									visible: true
								};
								// Perform an update on this user.
								Meteor.users.update(Meteor.user(), {
									$set: customerSubscription
								}, function (userUpdateError) {
									if (userUpdateError) {
										reject(userUpdateError);
									} else {
										// Once the subscription data has been added, return to Future.
										resolve(Meteor.user());
									}
								});
								const currentRoles = Roles.getRolesForUser(Meteor.userId());
								currentRoles.forEach((role) => {
									if (role === plan) {
										throw new Meteor.Error('400', 'User already subscribed to this plan');
									} else if (role === 'standard' || role === 'pro') {
										Roles.removeUsersFromRoles(Meteor.userId(), role);
									}
								});
								// add new subscription
								Roles.addUsersToRoles(Meteor.userId(), plan);
							} catch (exception) {
								reject(exception);
							}
						}
					});
				}
			});
		});
	},

	btCreateCustomer: function (nonce) {
		var user = Meteor.user();
		Meteor.call('btFindCustomer', user.customerId, function (error, result) {
			if (result) {
				return undefined;
			} else {
				var customerData = {
					email: user.email,
					paymentMethodNonce: nonce
				};
				return new Promise((resolve, reject) => {
					// Calling the Braintree API to create our customer!
					gateway.customer.create(customerData, function (customerCreateError, customerCreateResult) {
						if (customerCreateError) {
							reject(customerCreateError);
						} else {
							// If customer is successfuly created on Braintree servers,
							// we will now add customer ID to our User
							Meteor.users.update(user._id, {
								$set: {
									customerId: customerCreateResult.customer.id
								}
							});
							resolve(customerCreateResult);
						}
					});
				});
			}
		});
	},

	btFindCustomer: function (customerId) {
		return new Promise((resolve, reject) => {
			gateway.customer.find(customerId, function (error, result) {
				if (error) {
					reject(error);
				} else {
					resolve(result);
				}
			});
		});
	},

	btCreateSubscription: function (customerId, plan) {
		return new Promise((resolve, reject) => {
			// fetch customer data.
			Meteor.call('btFindCustomer', customerId, function (error, result) {
				if (error) {
					reject(error);
				} else {
					var subscriptionRequest = {
						paymentMethodToken: result.paymentMethods[0].token,
						planId: plan
					};

					// create subscription
					gateway.subscription.create(subscriptionRequest, function (subscriptionCreateError, subscriptionCreateResult) {
						if (subscriptionCreateError) {
							reject(subscriptionCreateError);
						} else {
							resolve(subscriptionCreateResult);
						}
					});
				}
			});
		});
	},

	btFindUserSubscription: function (customerId) {
		return new Promise((resolve, reject) => {
			// find customer
			gateway.customer.find(customerId, function (error, result) {
				if (error) {
					reject(error);
				} else {
					// get customer's last subscription
					var subscriptionId = result.paymentMethods[0].subscriptions;
					var last = subscriptionId.slice(-1)[0];

					resolve(last);
				}
			});
		});
	},

	btCancelSubscription: function () {
		var user = Meteor.userId();
		var getUser = Meteor.users.findOne({
			"_id": user
		}, {
			fields: {
				"customerId": 1
			}
		});

		return new Promise((resolve, reject) => {
			Meteor.call('btFindUserSubscription', getUser.customerId, function (error, customerSubscription) {
				if (error) {
					reject(error);
				} else {
					// cancel the active subscription
					gateway.subscription.cancel(customerSubscription.id, function (subscriptionCancelError) {
						if (subscriptionCancelError) {
							reject(subscriptionCancelError);
						} else {
							Meteor.users.update(Meteor.userId(), {
								$set: {
									"subscription.status": "Canceled",
									"subscription.ends": customerSubscription.paidThroughDate
								}
							}, function (userUpdateError, userUpdateResponse) {
								if (userUpdateError) {
									reject(userUpdateError);
								} else {
									resolve(userUpdateResponse);
								}
							});

							Roles.removeUsersFromRoles(Meteor.userId(), 'pro');
							Roles.addUsersToRoles(Meteor.userId(), 'standard');
						}
					});
				}
			});
		});
	}
});
